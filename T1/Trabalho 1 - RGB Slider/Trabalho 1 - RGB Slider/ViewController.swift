//
//  ViewController.swift
//  Trabalho 1 - RGB Slider
//
//  Created by Mauro Silva on 5/30/17.
//  Copyright © 2017 Mauro Silva. All rights reserved.
//
import UIKit

class ViewController: UIViewController {
    
    /*SLIDERS */
    var CORA=UISlider()
    var CORB=UISlider()
    var CORG=UISlider()
    var CORR=UISlider()
    
    /*RESULTADOS */
    var LBCOR=UILabel()
    var LBCODIGO=UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 1)
        setUI()
    }
    
    func setUI() {
        
        let LBTITULO = UILabel(frame: CGRect(x:0, y:5, width:view.frame.width, height:view.frame.height * 0.1))
        LBTITULO.text = "CHECK'DA'COLA"
        LBTITULO.textAlignment = .center
        view.addSubview(LBTITULO)
        
        LBCOR = UILabel (frame: CGRect(x: view.frame.maxX/2-75 , y: 75, width: 150, height: 150))
        LBCOR.backgroundColor = UIColor(colorLiteralRed: 100/255, green: 100/255, blue: 100/255, alpha: 1)
        view.addSubview(LBCOR)
        
        LBCODIGO = UILabel(frame:CGRect (x:0, y:245, width:view.frame.width, height:view.frame.height * 0.1))
        LBCODIGO.text = "R: 100 ~ G: 100 ~ B: 100 ~ A: 100"
        LBCODIGO.textAlignment = .center
        view.addSubview(LBCODIGO)
        
        /* CORES */
        CORR = UISlider (frame: CGRect(x: view.frame.maxX/2-150, y: 350, width: 300, height: 20))
        CORR.minimumValue = 0
        CORR.maximumValue = 255
        CORR.value = 150
        CORR.addTarget(self, action: #selector(ViewController.onChangeValueMySlider(sender:)), for: UIControlEvents.valueChanged)
        view.addSubview(CORR)
        
        CORG = UISlider (frame: CGRect(x: view.frame.maxX/2-150, y: 400 , width: 300, height: 20))
        CORG.minimumValue = 0
        CORG.maximumValue = 255
        CORG.value = 40
        CORG.addTarget(self, action: #selector(ViewController.onChangeValueMySlider(sender:)), for: UIControlEvents.valueChanged)
        view.addSubview(CORG)
        
        CORB = UISlider (frame: CGRect(x: view.frame.maxX/2-150, y: 450, width: 300, height: 20))
        CORB.minimumValue = 0
        CORB.maximumValue = 255
        CORB.value = 200
        CORB.addTarget(self, action: #selector(ViewController.onChangeValueMySlider(sender:)), for: UIControlEvents.valueChanged)
        view.addSubview(CORB)
        
        CORA = UISlider (frame: CGRect(x: view.frame.maxX/2-150, y: 500, width: 300, height: 20))
        CORA.minimumValue = 0
        CORA.maximumValue = 100
        CORA.value = 100
        CORA.addTarget(self, action: #selector(ViewController.onChangeValueMySlider(sender:)), for: UIControlEvents.valueChanged)
        view.addSubview(CORA)
    }
    func onChangeValueMySlider (sender: AnyObject){
        LBCOR.backgroundColor = UIColor(colorLiteralRed: CORR.value/255, green: CORG.value/255, blue: CORB.value/255, alpha: CORA.value/100)
        LBCODIGO.text = "R: \(Int(CORR.value)) ~ G: \(Int(CORG.value)) ~ B: \(Int(CORB.value)) ~ A: \(Int(CORA.value))"
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }}
