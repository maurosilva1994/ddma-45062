//
//  ViewController.swift
//  wot'the'numba?
//
//  Created by Mauro Silva on 6/19/17.
//  Copyright © 2017 Brains ON Networks. All rights reserved.
//
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var guessLabel: UILabel!
    @IBOutlet weak var guessText: UITextField!
    
    private var guessNumber = 0;
    private var countGuesses = 0;
    private var guessAgain = false;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guessNumber = Int(arc4random_uniform(100));
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func guessTheNumber(_ sender: AnyObject) {

        
        if guessAgain {
            countGuesses = 0;
            guessText.text = "";
            guessNumber = Int(arc4random_uniform(100));
            guessAgain = false;
            
        }
        
        if Int(guessText.text!) != nil {
            
            let num = Int(guessText.text!);
            countGuesses += 1;
            
            
            if num == guessNumber {
                guessLabel.text = "You got it! It took you \(countGuesses) to get it right!"
                guessAgain = true;
              
                
            }
            
            else if num! < guessNumber {
                guessLabel.text = "Too high!";
            }
            else if num! > guessNumber {
                guessLabel.text = "Too low!";
            }
            
            guessText.text = "";
        }
            
        else {
            guessLabel.text = "Please, enter a number in the field!"
            
}
    }
        }
